# Drone Project - Available Scripts

In the project directory, you can run:

### `yarn start:dev` / `npm start:dev`

Runs the app in the development mode.<br>
Open [http://localhost:6060](http://localhost:6060) to explore routes from a REST API client like Postman.

### `yarn start:build` / `npm start:build`

This creates a project build which will be located in the dist directory.

### `yarn start:prod` / `npm start:prod`

This runs the application in production mode.<br>
Open [http://localhost:6060](http://localhost:6060) to explore routes from a REST API client like Postman.

## Available Routes

From a REST API client like Postman, you can run:

---
### [HTTP Method = GET] <br/>`http://localhost:6060/drones/available`

This will list all the available drones, the assumption is that when the drone state is IDLE then its assumed to be available.

### [HTTP Method = POST] <br/>`http://localhost:6060/drones`

This will register a new drone with sample json data given below:<br/>
```
{
    "serial_number": "Mars",
    "model": "Middleweight",
    "weight": 500,
    "battery": 45,
    "state": "IDLE"
}
```

### [HTTP Method = POST] <br/>`http://localhost:6060/drones/1/load`

This loads medicine to the drone with id 1:<br/>
```
{
    "name": "Paracetamol",
    "weight": 200,
    "code": "ierwe",
    "image": "https://s3.amazonaws.com/ierwe"
}
```

### [HTTP Method = GET] <br/>`http://localhost:6060/drones/1/check-load`

This lists medicine loaded for drone 1

### [HTTP Method = GET] <br/>`http://localhost:6060/drones/1/check-battery`

This checks the battery percentage for drone 1

### [HTTP Method = PUT] <br/>`http://localhost:6060/drones/1/update-state`

This updates state for drone with id 1 to LOADED:<br/>
```
{
    "state": "LOADED"
}
```